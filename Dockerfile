FROM python:3.9-alpine

RUN apk add --update --no-cache curl nodejs npm
RUN apk add --no-cache aws-cli
RUN npm install -g serverless
RUN npm install --save-dev -g serverless-python-requirements
RUN python -m pip install --upgrade pip
RUN python -m pip install --upgrade build
RUN python -m pip install --upgrade pipenv
RUN apk add --no-cache terraform
